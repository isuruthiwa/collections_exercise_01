package com.nCinga;

public enum SortMethod {
    SortByRating,
    SortByReleaseYear
}
