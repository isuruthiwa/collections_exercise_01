package com.nCinga;

import com.sun.corba.se.spi.orbutil.fsm.Input;

import java.sql.Array;
import java.util.*;

import static com.nCinga.SortMethod.SortByRating;

public class Main {

    static ArrayList<Integer> sortedArray = new ArrayList<>();

    public static void arrayListSort(String[] args) {
        addElement(10);
        addElement(5);
        addElement(15);
        addElement(0);

        System.out.println(sortedArray);
    }

    public static void addElement(int element){
        sortedArray.add(element);
        Collections.sort(sortedArray);
    }

    public static ArrayList<Integer> removeDuplicates(ArrayList<Integer> arrayList){
        LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<>(arrayList);
        arrayList= new ArrayList<>(linkedHashSet);
        return arrayList;
    }

    public static void merge(String[] args) {
        ArrayList<Integer> arrayList= new ArrayList<>();

        arrayList.add(10);
        arrayList.add(10);
        arrayList.add(5);
        arrayList.add(10);
        arrayList.add(5);
        arrayList.add(10);

        System.out.println("Original ArrayList: "+ arrayList);

        ArrayList<Integer> dupList = new ArrayList<>();

        for (Integer element : arrayList) {
            if (!dupList.contains(element))
                dupList.add(element);
        }
        arrayList=dupList;
        System.out.println(arrayList);

        LinkedList<Integer> ll1 = new LinkedList<>();
        ll1.add(1);
        ll1.add(3);
        ll1.add(5);
        ll1.add(7);
        System.out.println(ll1);

        LinkedList<Integer> ll2 = new LinkedList<>();
        ll2.add(2);
        ll2.add(4);
        ll2.add(6);
        ll2.add(8);
        System.out.println(ll2);
        ll1.addAll(ll2);

        ListIterator<Integer> li = ll1.listIterator();
        for(Integer element: ll2) {
            while (li.hasNext()) {
                if (li.next()<element)
                    li.add(element);
            }
        }
//        Collections.sort(ll1);
        System.out.println("Final"+ll1);
    }

    public static LinkedList<Integer> sortByFrequency(LinkedList<Integer> ll) {
        HashMap<Integer, Integer> countMap = new HashMap();
        for (Integer element : ll) {
            if (countMap.get(element) == null)
                countMap.put(element, 1);
            else
                countMap.put(element, countMap.get(element) + 1);
        }

        System.out.println(countMap);
        LinkedList<Integer> ll_new = new LinkedList<>();
        for (int i = 0; i <countMap.size() ; i++) {
            int maxCount=0;
            int maxKey=0;
            for (Map.Entry entry : countMap.entrySet()) {
                if((int)entry.getValue()>maxCount) {
                    maxCount = (int) entry.getValue();
                    maxKey = (int) entry.getKey();
                }
            }
            if(maxCount>1){
                ll.removeAll(Collections.singleton(maxKey));
                for (int j = 0; j <maxCount ; j++) {
                    ll_new.addLast(maxKey);
                }
            }
            else {
                ll_new.addAll(ll);
                break;
            }
            countMap.replace(maxKey,0);
        }
        return ll_new;
    }

    public static void main(String[] args) {
        String exp = "[(])";
        if(operatorBalance(exp))
            System.out.println("Balanced");
        else
            System.out.println("Not Balanced");


        ArrayList<Movie> movies = new ArrayList<>();
        movies.add(new Movie("Sherlock",9.0,2015));
        movies.add(new Movie("Twillight",7.9,2010));
        movies.add(new Movie("Hulk",5.2,2012));
        movies.add(new Movie("Irishman",9.2,2019));

        Collections.sort(movies, new RatingComparator());
        System.out.println(movies);

        Collections.sort(movies, new ReleaseYearComparator());
        System.out.println(movies);

        movies.sort(Comparator.comparing(Movie::toString));
        System.out.println(movies);

        ArrayList<Integer> arrayList= new ArrayList<>(Arrays.asList(1,2,3,4,2,3,4,2,3,4,2,1));
        System.out.println(removeDuplicates(arrayList));

        LinkedList<Integer> linkedList= new LinkedList<>(arrayList);
        LinkedList<Integer> linkedList_New= new LinkedList<>(Arrays.asList(6,7,5,4,3,6,4,6,4,6,5));

//        System.out.println(linkedList.poll());
        System.out.println(linkedList);
        System.out.println(mergeLinkedLists(linkedList,linkedList_New));

        LinkedList<Integer> ll = new LinkedList<>(Arrays.asList(1, 5, 3, 6, 5, 2, 3, 8, 3, 7, 3));
        System.out.println(sortByFrequency(ll));

    }

    public static LinkedList<Integer> mergeLinkedLists(LinkedList<Integer> list1, LinkedList<Integer> list2){
        list1.sort(Comparator.naturalOrder());
        for(Integer elem: list2) {
            int i=0;

            while (i<list1.size()){
                if(list1.get(i)>elem || (i==list1.size()-1)) {
                    list1.add(i, elem);
                    break;
                }
                i++;
            }
        }
        return list1;
    }

    public static boolean operatorBalance(String exp) {
        ArrayList<String> operations= new ArrayList<>();
        operations.addAll(Arrays.asList(exp.split("")));
        System.out.println(operations);
        Stack<String> operators= new Stack<>();
        for(String element: operations){
            if(element.equals("(") || element.equals("[") ||element.equals("{")) {
                operators.push(element);
//                System.out.println(operators);
            }
            else if(element.equals(")")){
                if(!operators.pop().equals("("))
                    return false;
            }
            else if(element.equals("]")){
                if(!operators.pop().equals("["))
                    return false;
            }
            else if(element.equals("}")){
                if(!operators.pop().equals("{"))
                    return false;
            }
        }
        return true;
    }
}

