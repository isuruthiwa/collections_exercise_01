package com.nCinga;

import java.util.Comparator;

public class RatingComparator implements Comparator<Movie> {

    @Override
    public int compare(Movie movie, Movie t1) {
        if( movie.rating > t1.rating)
            return 1;
        if( movie.rating < t1.rating)
            return -1;
        return 0;
    }
}
