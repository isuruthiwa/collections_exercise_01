package com.nCinga;

public class Movie implements Comparable<Movie>{
    String name;
    Double rating;
    Integer releaseYear;

    public Movie(String name, Double rating, Integer releaseYear) {
        this.name = name;
        this.rating = rating;
        this.releaseYear = releaseYear;
    }

    public int compareTo(Movie movie, Enum<SortMethod> sortMethodEnum) {
        if(sortMethodEnum==SortMethod.SortByRating){
            if(rating>movie.rating)
                return 1;
            else if((rating<movie.rating))
                return -1;
            return 0;
        }
        else if(sortMethodEnum==SortMethod.SortByReleaseYear){
            if(releaseYear>movie.releaseYear)
                return 1;
            else if((releaseYear<movie.releaseYear))
                return -1;
            return 0;
        }
        return 0;
    }

    @Override
    public int compareTo(Movie movie) {
        return name.compareTo(movie.name);
    }



    @Override
    public String toString() {
        return "Movie{" +
                "name='" + name + '\'' +
                ", rating=" + rating +
                ", releaseYear=" + releaseYear +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(Integer releaseYear) {
        this.releaseYear = releaseYear;
    }
}
