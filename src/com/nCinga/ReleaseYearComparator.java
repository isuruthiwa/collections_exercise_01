package com.nCinga;

import java.util.Comparator;

public class ReleaseYearComparator implements Comparator<Movie> {
    @Override
    public int compare(Movie movie, Movie t1) {
        return movie.releaseYear-t1.releaseYear;
    }
}
